# App Server

- app-server-api : contains model classes.
- app-server-main : contains the spring boot application entrypoint.

## Compile

```
mvn clean compile
```

## Run unit tests

```
mvn clean test
```

## Create executable JAR test test

```
mvn clean package
```

## Install in local repository

```
mvn clean install
```