package fr.epsi.ci.demo;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class Cars {

    public static List<Car> getBestCars() {
        List<Car> cars = new LinkedList<>();
        Stream.of("Ferrari", "Jaguar", "Porsche", "Lamborghini", "Bugatti")
                .forEach(name -> {
            Car car = new Car();
            car.setName(name);
            cars.add(car);
        });
        return cars;
    }
}
