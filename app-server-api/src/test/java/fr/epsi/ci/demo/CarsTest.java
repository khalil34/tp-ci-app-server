package fr.epsi.ci.demo;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class CarsTest {

    @Test
    public void getBestCars() {
        List<Car> bestCars = Cars.getBestCars();
        assertEquals(5, bestCars.size());
        assertEquals("Ferrari", bestCars.get(0).getName());
        assertEquals("Jaguar", bestCars.get(1).getName());
        assertEquals("Porsche", bestCars.get(2).getName());
        assertEquals("Lamborghini", bestCars.get(3).getName());
        assertEquals("Bugatti", bestCars.get(4).getName());
    }

}