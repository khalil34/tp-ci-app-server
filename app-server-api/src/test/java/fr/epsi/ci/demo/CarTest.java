package fr.epsi.ci.demo;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CarTest {

    Car car;

    @Before
    public void setUp() {
        this.car = new Car();
        this.car.setName("Ferrari");
        this.car.setId(4L);
    }

    @Test
    public void getId() {
        assertEquals(4, car.getId().intValue());
    }

    @Test
    public void getName() {
        assertEquals("Ferrari", car.getName());
    }
}